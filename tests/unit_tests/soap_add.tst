// This file is released into the public domain
//=================================
// load toolbox_skeleton
test_dir = get_absolute_file_path('soap_add.tst');
if funptr('soap_add') == 0 then
  root_tlbx_path = test_dir+'../../';
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
if or(soap_add('localhost',1080,[1 2 3],[9 8 7]) <> [10 10 10]) then pause,end
if or(soap_add('localhost',1080,[1+9*%i 2+8*%i 3+7*%i],[9+%i 8+2*%i 7+3*%i]) <> [10+10*%i 10+10*%i 10+10*%i]) then pause,end
if or(soap_add('localhost',1080,[%F %F %T],[%T %F %T]) <> [%T %F %T]) then pause,end

// 0 + 0 0 0 0
// 0 0 0 0 0 0
// 0 0 0 + 0 0
// 0 0 0 0 0 0
// 0 0 0 0 0 +
A=sparse([1,2;3,4;5,6],[%T,%T,%T]);

// 0 0 0 0 0 0
// 0 0 0 0 0 +
// 0 0 0 0 0 0
// 0 0 0 0 0 +
// 0 0 0 0 0 +
B=sparse([2,6;4,6;5,6],[%T,%T,%T]);

C=[%F %T %F %F %F %F
   %F %F %F %F %F %T
   %F %F %F %T %F %F
   %F %F %F %F %F %T
   %F %F %F %F %F %T];
if or(soap_add('localhost',1080,A,B) <> C) then pause,end

// 0 1.2 0 0   0 0
// 0 0   0 0   0 0
// 0 0   0 2.4 0 0
// 0 0   0 0   0 0
// 0 0   0 0   0 3.6
A=sparse([1,2;3,4;5,6],[1.2 2.4 3.6]);

// 0 0 0 0 0 0
// 0 0 0 0 0 5.6
// 0 0 0 0 0 0
// 0 0 0 0 0 2.8
// 0 0 0 0 0 1.4
B=sparse([2,6;4,6;5,6],[5.6 2.8 1.4]);

C=[0 1.2 0 0   0 0
   0 0   0 0   0 5.6
   0 0   0 2.4 0 0
   0 0   0 0   0 2.8
   0 0   0 0   0 5.0];
if or(soap_add('localhost',1080,A,B) <> C) then pause,end

// 0 1.2i 0 0    0 0
// 0 0    0 0    0 0
// 0 0    0 2.4i 0 0
// 0 0    0 0    0 0
// 0 0    0 0    0 3.6i
A=sparse([1,2;3,4;5,6],[1.2*%i 2.4*%i 3.6*%i]);

// 0 0 0 0 0 0
// 0 0 0 0 0 5.6
// 0 0 0 0 0 0
// 0 0 0 0 0 2.8
// 0 0 0 0 0 1.4
B=sparse([2,6;4,6;5,6],[5.6 2.8 1.4]);

C=[0 1.2*%i 0 0      0 0
   0 0      0 0      0 5.6
   0 0      0 2.4*%i 0 0
   0 0      0 0      0 2.8
   0 0      0 0      0 1.4+3.6*%i];
if or(soap_add('localhost',1080,A,B) <> C) then pause,end
//=================================
