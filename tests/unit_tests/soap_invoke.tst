// This file is released into the public domain
//=================================
// load toolbox_skeleton
test_dir = get_absolute_file_path('soap_invoke.tst');
if funptr('soap_invoke') == 0 then
  root_tlbx_path = test_dir+'../../';
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
endpoint = 'http://www.webservicex.net/ConvertTemperature.asmx';
action = 'http://www.webserviceX.NET/ConvertTemp';
request = '<ConvertTemp xmlns=""http://www.webserviceX.NET/"">' + ..
              '<Temperature>36.6</Temperature>' + ..
              '<FromUnit>degreeCelsius</FromUnit>' + ..
              '<ToUnit>degreeFahrenheit</ToUnit>' + ..
          '</ConvertTemp>';

response = soap_invoke(endpoint, action, request);
disp(response);

response = soap_invoke(endpoint, action, request, 'x');
disp(response);

response = soap_invoke(endpoint, action, request, 't');
disp(response);

request = tlist(['ConvertTemp' '#namespace' 'Temperature' 'FromUnit' 'ToUnit'], 'http://www.webserviceX.NET/',..
  tlist(['Temperature' '#content'],'36.6'),..
  tlist(['FromUnit' '#content'],'degreeCelsius'),..
  tlist(['ToUnit' '#content'],'degreeFahrenheit'));

response = soap_invoke(endpoint, action, request);
disp(response);

response = soap_invoke(endpoint, action, request, 'x');
disp(response);

response = soap_invoke(endpoint, action, request, 't');
disp(response);


clear endpoint;
clear action;
clear request;
clear response;
//=================================
