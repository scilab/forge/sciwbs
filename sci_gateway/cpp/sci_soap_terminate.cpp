/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "SOAP.h"
#include "../../src/cpp/soap_server_exceptions.h"
#include "soap_argument_utilities.h"
#include "soap_error_codes.h"

/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"

    /**
     * The gateway function for soap_terminate()
     * @param[in] fname the name of the file for the error messages
     * @return 0 if successful, a negative value otherwise
     */
    int sci_soap_terminate(char *fname)
    {
        // the number of input parameters must be 1 or 0
        CheckRhs(0, 1);

        int id;

        if (Rhs > 0)
        {
            double val = 0;

            // read the argument - the id of the server to be terminated
            if (getScalarDoubleAtPosition(1, &val))
            {
                // no return variable
                LhsVar(1) = 0;

                // put the value on the stack
                PutLhsVar();

                return 0;
            }

            id = (int)val;
        }

        try
        {
            if (Rhs > 0)
            {
                SOAP::Instance().terminate(id);
            }
            else
            {
                SOAP::Instance().terminate();
            }
        }
        catch (NoServerRunningException)
        {
            Scierror(SERVER_NOT_FOUND_ERROR, "%s: No server is running.\n", fname);
        }
        catch (ServerNotFoundException e)
        {
            Scierror(SERVER_NOT_FOUND_ERROR, "%s: Server with ID %d not found.\n", fname, e.getServerId());
        }
        catch (MultipleServersRunningException)
        {
            Scierror(MULTIPLE_SERVERS_ERROR, "%s: %s\n%s\n", fname,
                "Multiple SOAP servers are running:", "You must choose one of the servers.");
        }
        catch (ServerTerminationException)
        {
            if (Rhs == 0)
            {
                Scierror(SERVER_TERMINATION_FAILURE, "Default server termination failed.\n", fname);
            }
            else
            {
                Scierror(SERVER_TERMINATION_FAILURE, "Server %d termination failed.\n", fname, id);
            }
        }

        // no return variable
        LhsVar(1) = 0;

        // put the value on the stack
        PutLhsVar();

        return 0;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */
