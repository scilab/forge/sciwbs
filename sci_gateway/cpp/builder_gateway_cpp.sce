// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

if getos() == 'Windows' then
  // to manage long pathname
  includes_src_cpp = '-I""' + get_absolute_file_path('builder_gateway_cpp.sce') + '../../src/cpp"" ' + ..
                   '-I""' + get_absolute_file_path('builder_gateway_cpp.sce') + '""' + ' ' + ..
                   '-I""' + SCI + '/modules/core/src/c' + '"" ' + ..
                   '-I""' + SCI + '/modules/functions/src/c' + '"" ' + ..
                   '-I""' + SCI + '/modules/api_scilab/src/cpp' + '"" ' + ..
                   '-I""' + SCI + '/modules/call_scilab/includes' + '"" ' + ..
                   '-I/usr/include/libxml2 ';
else
  includes_src_cpp = '-I' + get_absolute_file_path('builder_gateway_cpp.sce') + '../../src/cpp ' + ..
                   '-I' + get_absolute_file_path('builder_gateway_cpp.sce') + ' ' + ..
                   '-I' + SCI + '/modules/core/src/c ' + ..
                   '-I' + SCI + '/modules/functions/src/c ' + ..
                   '-I' + SCI + '/modules/api_scilab/src/cpp ' + ..
                   '-I' + SCI + '/modules/call_scilab/includes ' + ..
                   '-I/usr/include/libxml2 ';
end

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

functionMatches = ['soap_add', 'sci_soap_add'; 'soap_run', 'sci_soap_run'; ..
                   'soap_terminate', 'sci_soap_terminate'; 'soap_servers', 'sci_soap_servers'; ..
                   'soap_export', 'sci_soap_export'; 'soap_unexport', 'sci_soap_unexport'; ..
                   'soap_list_functions', 'sci_soap_list_functions'; 'soap_invoke', 'sci_soap_invoke'];

files = ['sci_soap_add.cpp', 'sci_soap_run.cpp', 'sci_soap_terminate.cpp', 'soap_argument_utilities.cpp', ..
         'sci_soap_servers.cpp', 'sci_soap_export.cpp', 'sci_soap_unexport', 'soap_scilab_utilities.cpp', ..
         'sci_soap_list_functions.cpp', 'sci_soap_invoke.cpp'];

tbx_build_gateway('sciwbs_c', functionMatches, files, get_absolute_file_path('builder_gateway_cpp.sce'), ..
                  ['../../src/cpp/librun'], '', includes_src_cpp);

clear WITHOUT_AUTO_PUTLHSVAR;

clear tbx_build_gateway;
