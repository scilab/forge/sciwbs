/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "invoke.h"
#include "soap_argument_utilities.h"
#include "soap_error_codes.h"
#include "utilities.h"


static const char* SOAP_ENVELOPE_NAMESPACE = "http://schemas.xmlsoap.org/soap/envelope/";
static const char* SOAP_XSI_NAMESPACE = "http://www.w3.org/1999/XMLSchema-instance";
static const char* SOAP_XSD_NAMESPACE = "http://www.w3.org/1999/XMLSchema";
static const char* SOAP_ENVELOPE_ENCODING = "http://schemas.xmlsoap.org/soap/encoding/";

static const char* ATTRIBUTES_FIELD_NAME = "#attributes";
static const char* NAMESPACE_FIELD_NAME = "#namespace";
static const char* CONTENT_FIELD_NAME = "#content";

static int processString(const char * const fname, const char * const endpoint, const char * const action, 
    const char * const request);
static int processXML(const char * const fname, const char * const endpoint, const char * const action, 
    int * const request);

static int countChildren(xmlNode * node);
static int countAttributes(xmlNode* node);

static int deserializeTList(xmlNode* node, int varNum, int* parent, int position, int** pList);
static int deserializeContentElement(xmlNode* node, int varNum, int* parent, int position);

static xmlNode* serializeString(int* addr);
static xmlNode* serializeTList(int* addr);

static int putNamespace(xmlNode* node, int* addr);
static int putAttributes(xmlNode* node, int* addr);

static xmlDoc* formRequestFromString(char* str);
static xmlDoc* formRequestFromList(int* addr);
static xmlDoc* formRequest(xmlNode* body);

int getParse(char* fname);
xmlDoc* getRequest(char* fname);
int assignResult(char* fname, xmlDoc* responseDoc, int parse);

/* ==================================================================== */
extern "C"
{
    #include "stack-c.h"
    #include "api_scilab.h"
    #include "Scierror.h"
    #include "MALLOC.h"
    #include "sci_types.h"

    #undef getType

    int sci_soap_invoke(char *fname)
    {
        char* endpoint       = NULL;
        char* action         = NULL;

        // check that we have between 3 and 4 input parameters
        // check that we have only 1 parameters output
        CheckRhs(3, 4);
        CheckLhs(1, 1);

        // get the first argument (the end point)
        if (getSingleStringAtPosition(1, &endpoint))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        // get the second argument (the action)
        if (getSingleStringAtPosition(2, &action))
        {
            // no return variable
            LhsVar(1) = 0;

            // put the value on the stack
            PutLhsVar();

            return 0;
        }

        int parse = getParse(fname);
        if (parse < 0)
        {
            // release the memory for the allocated strings
            if (endpoint != NULL)
            {
                freeAllocatedSingleString(endpoint);
            }
            if (action != NULL)
            {
                freeAllocatedSingleString(action);
            }

            // put the variable on the stack
            PutLhsVar();

            return 0;
        }

        xmlDoc* requestDoc = getRequest(fname);
        if (requestDoc == NULL)
        {
            // release the memory for the allocated strings
            if (endpoint != NULL)
            {
                freeAllocatedSingleString(endpoint);
            }
            if (action != NULL)
            {
                freeAllocatedSingleString(action);
            }

            // put the variable on the stack
            PutLhsVar();

            return 0;
        }

        xmlDoc* responseDoc;
        // invoke the service
        if (invoke(endpoint, action, requestDoc, &responseDoc))
        {
            Scierror(REMOTE_CALL_ERROR, "%s: Remote invocation failed.\n", fname);

            // release the memory for the allocated strings
            if (endpoint != NULL)
            {
                freeAllocatedSingleString(endpoint);
            }
            if (action != NULL)
            {
                freeAllocatedSingleString(action);
            }

            // no return variable
            LhsVar(1) = 0;

            // put the variable on the stack
            PutLhsVar();

            return 0;
        }

        if (assignResult(fname, responseDoc, parse))
        {
            // release the memory for the allocated strings
            if (endpoint != NULL)
            {
                freeAllocatedSingleString(endpoint);
            }
            if (action != NULL)
            {
                freeAllocatedSingleString(action);
            }

            // put the variable on the stack
            PutLhsVar();

            return 0;
        }
        
        // release the memory allocated for the request document
        //xmlFreeDoc(requestDoc);

        // release the memory allocated inside invoke()
        //xmlFreeDoc(responseDoc);

        // release the memory for the allocated strings
        if (endpoint != NULL)
        {
            freeAllocatedSingleString(endpoint);
        }
        if (action != NULL)
        {
            freeAllocatedSingleString(action);
        }

        LhsVar(1) = Rhs + 1;

        // put the variable on the stack
        PutLhsVar();

        return 0;
    }
/* ==================================================================== */
} /* extern "C" */
/* ==================================================================== */

xmlDoc* getRequest(char* fname)
{
    // get the third argument (the request string or tlist)
    int* piRequest = NULL;
    int type;
    SciErr sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piRequest);
    if (sciErr.iErr)
    {
        return NULL;
    }

    sciErr = getVarType(pvApiCtx, piRequest, &type);
    if (sciErr.iErr)
    {
        return NULL;
    }

    xmlDoc* requestDoc = NULL;
    switch (type)
    {
        case sci_strings:
        {
            // get the dimensions
            int m1;
            int n1;
            sciErr = getVarDimension(pvApiCtx, piRequest, &m1, &n1);
            if (sciErr.iErr)
            {
                return NULL;
            }

            char* requestStr;
            if (getSingleStringAtPosition(3, &requestStr))
            {
                return NULL;
            }

            requestDoc = formRequestFromString(requestStr);

            freeAllocatedSingleString(requestStr);

            break;
        }
        case sci_tlist:
        {
            requestDoc = formRequestFromList(piRequest);

            break;
        }
        default:
        {
            Scierror(INVALID_ARGUMENT_TYPE, "%s: Invalid type of argument %d: a single string or a tlist expected.\n", fname, 3);
            return 0;
        }
    }

    return requestDoc;
}

int getParse(char* fname)
{
    if (Rhs > 3)
    {
        char* val = NULL;
        // get the fourth argument (the output representation)
        if (getSingleStringAtPosition(4, &val))
        {
            return -1;
        }

        int parse = -1;
        if (!strcmp(val, "x"))
        {
            parse = 1;
        }
        else if (!strcmp(val, "t"))
        {
            parse = 0;
        }
        else
        {
            Scierror(INVALID_ARGUMENT_VALUE, "%s: Invalid value of argument %d: %s expected.\n", fname, 4, "\'x\' or \'t\'");
        }
        freeAllocatedSingleString(val);
        return parse;
    }
    else if (Rhs == 3)
    {
        int* piRequest = NULL;
        int type;
        SciErr sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piRequest);
        if (sciErr.iErr)
        {
            return -1;
        }

        sciErr = getVarType(pvApiCtx, piRequest, &type);
        if (sciErr.iErr)
        {
            return -1;
        }

        switch (type)
        {
            case sci_strings:
            {
                return 0;
                // break;
            }
            case sci_tlist:
            {
                return 1;
                // break;
            }
            default:
            {
                return -1;
                // break;
            }
        }
    }
}

int assignResult(char* fname, xmlDoc* responseDoc, int parse)
{
    if (parse == 0)
    {
        // Print out result
        int size;
        char* str = NULL;
        xmlDocDumpFormatMemory(responseDoc, (xmlChar**)&str, &size, 1);

        if (createSingleString(pvApiCtx, Rhs + 1, str))
        {
            return -1;
        }

        if (str != NULL)
        {
            free(str);
        }
    }
    else
    {
        int * pList;
        xmlNode * rootElement = xmlDocGetRootElement(responseDoc);
        if (deserializeTList(rootElement, Rhs + 1, NULL, 0, &pList))
        {
            Scierror(XML_BUILDING_ERROR, "%s: deserialization failed.\n", fname);
            return -1;
        }
    }
    return 0;
}

/**
 * Creates a SOAP request from a string
 * @param[in] str the string containing the request
 * @return a pointer to the XML document containing the request
 */
xmlDoc* formRequestFromString(char* str)
{
    xmlDocPtr bodyXML;

    if (!(bodyXML = xmlParseDoc(BAD_CAST str)))
    {
        Scierror(XML_BUILDING_ERROR, "%s: could not parse the provided string.\n", "formRequestFromString");
        return NULL;
    }

    return formRequest(xmlDocGetRootElement(bodyXML));
}

/**
 * Creates a SOAP request extracting information from a Scilab tlist
 * @param[in] addr the address of the Scilab tlist
 * @return a pointer to the XML document containing the request
 */ 
xmlDoc* formRequestFromList(int* addr)
{
    xmlNode* body;
    if (!(body = serializeTList(addr)))
    {
        Scierror(XML_BUILDING_ERROR, "%s: could not extract information from the provided tlist.\n", "formRequestFromList");
        return NULL;
    }

    return formRequest(body);
}

/**
 * Creates a SOAP request based on the provided body strucuture
 * @param[in] body the pointer to the element to be added to the body of the request
 * @return a pointer to the XML document containing the request
 */
xmlDoc* formRequest(xmlNode* body)
{
    xmlDocPtr doc = xmlNewDoc(BAD_CAST "1.0");
    xmlNodePtr envNode = xmlNewNode(NULL, BAD_CAST "Envelope");
    xmlDocSetRootElement(doc, envNode);
    xmlNsPtr nsEnv = xmlNewNs(envNode, BAD_CAST SOAP_ENVELOPE_NAMESPACE, BAD_CAST "SOAP-ENV");
    xmlSetNs(envNode, nsEnv);
    xmlNsPtr nsXSI = xmlNewNs(envNode, BAD_CAST SOAP_XSI_NAMESPACE, BAD_CAST "xsi");
    xmlNsPtr nsXSD = xmlNewNs(envNode, BAD_CAST SOAP_XSD_NAMESPACE, BAD_CAST "xsd");
    xmlAttrPtr attrEncStyle = xmlNewNsProp(envNode, nsEnv, BAD_CAST "encodingStyle", BAD_CAST SOAP_ENVELOPE_ENCODING);

    xmlNodePtr headerNode = xmlNewChild(envNode, nsEnv, BAD_CAST "Header", NULL);
    xmlNodePtr bodyNode = xmlNewChild(envNode, nsEnv, BAD_CAST "Body", NULL);

    xmlAddChild(bodyNode, body);

    return doc;
}

/**
 * Processes an XML element and builds the corresponding Scilab tlist
 * @param[in] node the XML element to get information from
 * @param[in] varNum the variable number of the tlist being built
 * @param[in] parent the address of the tlist to add an element to
 * @param[in] position the position in the parent tlist to add the new element at
 * @param[out] the pointer to the newly created Scilab tlist
 * @return 0 if successful, a negative value otherwise
 */
int deserializeTList(xmlNode* node, int varNum, int* parent, int position, int** pList)
{
    SciErr sciErr;

    // count children
    const int childrenCount = countChildren(node);
    const int fieldCount = childrenCount + 3;

    // create a list with appropriate number of items (number of children + 1 item for names)
    if (parent == NULL)
    {
        sciErr = createTList(pvApiCtx, varNum, fieldCount, pList);
        if (sciErr.iErr)
        {
            Scierror(XML_PARSING_ERROR, "%s: could not create a tlist.\n", "deserializeTList");
            return -1;
        }
    }
    else
    {
        sciErr = createTListInList(pvApiCtx, varNum, parent, position, fieldCount, pList);
        if (sciErr.iErr)
        {
            Scierror(XML_PARSING_ERROR, "%s: could not create a tlist.\n", "deserializeTList");
            return -1;
        }
    }    

    // allocate memory for names array
    const char** names = (const char**)malloc(sizeof(char*) * fieldCount);

    // set the first name
    names[0] = (char*)node->name;

    names[1] = NAMESPACE_FIELD_NAME;
    names[2] = ATTRIBUTES_FIELD_NAME;

    // fill the names array
    int i = 3;
    for (xmlNode* curNode = node->children; curNode; curNode = curNode->next)
    {
        switch (curNode->type)
        {
            case XML_ELEMENT_NODE:
            {
                names[i] = (char*)curNode->name;
                break;
            }
            case XML_TEXT_NODE:
            {
                names[i] = CONTENT_FIELD_NAME;
                break;
            }
        }
    }

    // set the names matrix
    sciErr = createMatrixOfStringInList(pvApiCtx, varNum, *pList, 1, 1, fieldCount, names);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not create the names field in a tlist.\n", "deserializeTList");
        return -1;
    }

    // get the namespace
    const char* href = (const char*)node->ns->href;
    sciErr = createMatrixOfStringInList(pvApiCtx, varNum, *pList, 2, 1, 1, &href);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not create the namespace field in a tlist.\n", "deserializeTList");
        return -1;
    }

    // get the attributes
    const int attrCount = countAttributes(node);
    char** attributes = new char*[attrCount * 3];
    for (int i = 0; i < attrCount; i++)
    {
        xmlAttr* attr = node->properties;
        attributes[i] = (char*)attr->ns->href;
        attributes[attrCount + i] = (char*)attr->name;
        attributes[attrCount * 2 + i] = (char*)attr->children->content;
        attr = attr->next;
    }
    sciErr = createMatrixOfStringInList(pvApiCtx, varNum, *pList, 3, attrCount, 3, attributes);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not create the attributes field in a tlist.\n", "deserializeTList");
        return -1;
    }
    delete[]attributes;

    // process children
    i = 4;
    for (xmlNode* curNode = node->children; curNode; curNode = curNode->next)
    {
        if (curNode->type == XML_ELEMENT_NODE)
        {
            int* childAddr;
            if (deserializeTList(curNode, varNum, *pList, i, &childAddr))
            {
                Scierror(XML_PARSING_ERROR, "%s: could not deserialize a child element.\n", "deserializeTList");
                return -1;
            }
        }
        else if (curNode->type == XML_TEXT_NODE)
        {
            if (deserializeContentElement(curNode, varNum, *pList, i))
            {
                Scierror(XML_PARSING_ERROR, "%s: could not deserialize a content element.\n", "deserializeTList");
                return -1;
            }
        }
    }
    return 0;
}

/**
 * Processes an XML element and adds content to an element of a tlist
 * @param[in] node the XML element to get information from
 * @param[in] varNum the variable number of the tlist being built
 * @param[in] parent the address of the tlist to add an element to
 * @param[in] position the position in the parent tlist to add the new element at
 * @return 0 if successful, a negative value otherwise
 */
int deserializeContentElement(xmlNode* node, int varNum, int* parent, int position)
{
    SciErr sciErr = createMatrixOfStringInList(pvApiCtx, varNum, parent, position, 1, 1, (char**)&node->content);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not create a matrix of string.\n", "deserializeContentElement");
        return -1;
    }
    return 0;
}

/**
 * Counts the number of children nodes of element and text types.
 * @param[in] node the node whose children are to be calculated
 * @return the number of element and text sub-nodes
 */
int countChildren(xmlNode * node)
{
    int count = 0;
    for (xmlNode* curNode = node->children; curNode; curNode = curNode->next)
    {
        if (curNode->type == XML_ELEMENT_NODE || curNode->type == XML_TEXT_NODE)
        {
            count++;
        }
    }
    return count;
}

/**
 * Counts the number of attributes.
 * @param[in] node the node whose attributes are to be calculated
 * @return the number of attributes associated with the given XML node
 */
int countAttributes(xmlNode* node)
{
    int count = 0;
    for (xmlAttr* attr = node->properties; attr; attr = attr->next)
    {
        if (attr->type == XML_ATTRIBUTE_NODE)
        {
            count++;
        }
    }
    return count;
}

/**
 * Creates an XML text node based on the contents of a Scilab string matrix
 * @param[in] addr the address of the string matrix
 * @return a pointer to the newly created text node 
 */
xmlNode* serializeString(int* addr)
{
    char* str = NULL;
    if (getAllocatedSingleString(pvApiCtx, addr, &str))
    {
        Scierror(XML_BUILDING_ERROR, "%s: could not retrieve the single string.\n", "serializeString");
        return NULL;
    }

    xmlNode* textNode = xmlNewText(BAD_CAST str);

    freeAllocatedSingleString(str);

    return textNode;
}

/**
 * Creates an XML element based on the contents of a Scilab tlist
 * @param[in] addr the address of the tlist
 * @return a pointer to the newly created XML element
 */
xmlNode* serializeTList(int* addr)
{
    // check that the variable is a tlist
    int type;
    SciErr sciErr = getVarType(pvApiCtx, addr, &type);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not get type of the variable.\n", "serializeTList");
        return NULL;
    }
    if (type != sci_tlist)
    {
        Scierror(XML_PARSING_ERROR, "%s: wrong type of the variable.\n", "serializeTList");
        return NULL;
    }

    // get the first element of the tlist - the names of the fields
    int* elementAddr = NULL;
    sciErr = getListItemAddress(pvApiCtx, addr, 1, &elementAddr);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not get list item address.\n", "serializeTList");
        return NULL;
    }

    int rows = 0;
    int cols = 0;
    char** strings;
    if (getAllocatedMatrixOfString(pvApiCtx, elementAddr, &rows, &cols, &strings))
    {
        Scierror(XML_PARSING_ERROR, "%s: could not retrieve the matrix of string.\n", "serializeTList");
        return NULL;
    }

    // create the XML element
    xmlNode* node = xmlNewNode(NULL, BAD_CAST strings[0]);

    int items = 0;
    sciErr = getListItemNumber(pvApiCtx, addr, &items);
    if (sciErr.iErr)
    {
        Scierror(XML_PARSING_ERROR, "%s: could not get the number of items in the list.\n", "serializeTList");
        freeAllocatedMatrixOfString(rows, cols, strings);
        return NULL;
    }

    int namespaceFieldCount = 0;
    int attributesFieldCount = 0;

    // add children
    for (int i = 1; i < items; i++)
    {
        sciErr = getListItemAddress(pvApiCtx, addr, i + 1, &elementAddr);
        if (sciErr.iErr)
        {
            Scierror(XML_PARSING_ERROR, "%s: could not get the address of the variable.\n", "serializeTList");
            freeAllocatedMatrixOfString(rows, cols, strings);
            return NULL;
        }

        char* name = strings[i];
        if (!strcmp(name, NAMESPACE_FIELD_NAME))
        {
            if (namespaceFieldCount != 0)
            {
                Scierror(XML_PARSING_ERROR, "%s: more than one namespace field found.\n", "serializeTList");
                return NULL;
            }

            if (putNamespace(node, elementAddr))
            {
                Scierror(XML_PARSING_ERROR, "%s: could not assing namespace to an element.\n", "serializeTList");
                freeAllocatedMatrixOfString(rows, cols, strings);
                return NULL;
            }
            namespaceFieldCount++;
            continue;
        }
        else if (!strcmp(name, ATTRIBUTES_FIELD_NAME))
        {
            if (attributesFieldCount != 0)
            {
                Scierror(XML_PARSING_ERROR, "%s: more than one attributes field found.\n", "serializeTList");
                return NULL;
            }
            if (putAttributes(node, elementAddr))
            {
                Scierror(XML_PARSING_ERROR, "%s: could not assign attributes to an element.\n", "serializeTList");
                freeAllocatedMatrixOfString(rows, cols, strings);
                return NULL;
            }
            attributesFieldCount++;
            continue;
        }
        else if (!strcmp(name, CONTENT_FIELD_NAME))
        {
            xmlNode* childNode = serializeString(elementAddr);
            if (childNode == NULL)
            {
                Scierror(XML_PARSING_ERROR, "%s: could not serialize the content element.\n", "serializeTList");
                freeAllocatedMatrixOfString(rows, cols, strings);
                return NULL;
            }
            xmlAddChild(node, childNode);
            continue;
        }

        // recursively serialize the child element
        xmlNode* childNode = serializeTList(elementAddr);
        if (childNode == NULL)
        {
            Scierror(XML_PARSING_ERROR, "%s: could not serialize a nested element.\n", "serializeTList");
            freeAllocatedMatrixOfString(rows, cols, strings);
            return NULL;
        }

        // add the child node
        xmlAddChild(node, childNode);
    }

    freeAllocatedMatrixOfString(rows, cols, strings);
    return node;
}

/**
 * Adds namespace declarations and sets the namespace of an XML element in accordance with
 * the content of a Scilab string matrix
 * @param[in] node the XML element to get the namespace declarations
 * @param[in] addr the address of a Scilab string matrix
 * @return 0 if successful, a negative value otherwise
 */
int putNamespace(xmlNode* node, int* addr)
{
    char** content = NULL;
    int rows = 0;
    int cols = 0;
    if (getAllocatedMatrixOfString(pvApiCtx, addr, &rows, &cols, &content))
    {
        Scierror(XML_PARSING_ERROR, "%s: could not retrieve the matrix of string.\n", "putNamespace");
        return -1;
    }

    if (rows != 1 || cols < 1 || cols > 2)
    {
        Scierror(XML_PARSING_ERROR, "%s: the matrix of string is of the wrong size.\n", "putNamespace");
        freeAllocatedMatrixOfString(rows, cols, content);
        return -1;
    }

    xmlNs* ns = xmlNewNs(node, BAD_CAST content[0], NULL);

    xmlSetNs(node, ns);

    freeAllocatedMatrixOfString(rows, cols, content);

    return 0;
}

/**
 * Adds attributes to an XML element in accordance with the content of a Scilab
 * string matrix
 * @param[in] node the XML element whose attributes are set
 * @param[in] addr the address of a Scilab string matrix
 * @return 0 if successful, a negative value otherwise
 */
int putAttributes(xmlNode * node, int * addr)
{
    char** content = NULL;
    int rows = 0;
    int cols = 0;
    if (getAllocatedMatrixOfString(pvApiCtx, addr, &rows, &cols, &content))
    {
        Scierror(XML_PARSING_ERROR, "%s: could not retrieve the matrix of string.\n", "putAttributes");
        return -1;
    }

    if (rows < 1 || cols != 2)
    {
        Scierror(XML_PARSING_ERROR, "%s: the matrix of string is of a wrong size.\n", "putAttributes");
        freeAllocatedMatrixOfString(rows, cols, content);
        return -1;
    }

    for (int i = 0; i < rows; i++)
    {
        if (xmlNewProp(node, BAD_CAST content[i], BAD_CAST content[2 * i + 1]) == NULL)
        {
            Scierror(XML_PARSING_ERROR, "%s: could not assign an attribute.\n", "putAttributes");
            freeAllocatedMatrixOfString(rows, cols, content);
            return -1;
        }
    }

    freeAllocatedMatrixOfString(rows, cols, content);
    return 0;
}
