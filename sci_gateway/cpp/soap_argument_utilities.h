/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_ARGUMENT_UTILITIES_H__
#define __SOAP_ARGUMENT_UTILITIES_H__

/*
 * Retrieves the value of a double argument at a given position
 * @param[in] position the number of the right-hand side argument to be retrieved
 * @param[out] val the pointer to the variable where the value will be stored
 * @return 0 if successful, a negative value otherwise
 */
int getScalarDoubleAtPosition(int position, double* val);

/*
 * Retrieves the value of a string argument at a given position
 * @param[in] position the number of the right-hand side argument to be retrieved
 * @param[out] pstr the pointer to the variable where the value will be stored
 * @return 0 if successful, a negative value otherwise
 */
int getSingleStringAtPosition(int position, char** str);

/*
 * Retrieves the values of a double matrix argument at a given position
 * @param[in] position the number of the right-hand side argument to be retrieved
 * @param[out] pstr the pointer to the array to be filled
 * @return 0 if successful, a negative value otherwise
 */
int getDoubleMatrixAtPosition(int position, int* rows, int* cols, double** values);

/*
 * Retrieves the values of a string matrix argument at a given position
 * @param[in] position the number of the right-hand side argument to be retrieved
 * @param[out] pstr the pointer to the variable where the pointer to the array
 * @return 0 if successful, a negative value otherwise
 */
int getStringMatrixAtPosition(int position, int* rows, int* cols, char*** strings);

/*
 * Checks if the variable at position <position> on stack is actually a string
 * and gets its values, printing error messages if not successful
 * @param[in] fname the name of the file passed from the gateway function
 * @param[in] position the number of the right-hand side argument to be queried
 * @param[out] str the string read
 * @return 0 if successful, 1 if the paramters is an empty matrix, a negative value otherwise
 */
int checkAndGetSingleString(char *fname, int position, char** str);

#endif /*__SOAP_ARGUMENT_UTILITIES_H__*/
