/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_ERROR_CODES_H__
#define __SOAP_ERROR_CODES_H__

const int NOT_A_FUNCTION_ERROR         = 7001;
const int MULTIPLE_ENTRIES_ERROR       = 7002;
const int SERVER_NOT_FOUND_ERROR       = 7003;
const int EXPORT_FAILURE               = 7004;
const int MULTIPLE_SERVERS_ERROR       = 7005;

const int SOAP_SERVER_STARTUP_FAILURE  = 7006;
const int INVALID_PORT_NUMBER          = 7007;
const int ERROR_READING_ARGUMENT       = 7008;
const int INVALID_ARGUMENT_TYPE        = 7009;
const int INVALID_ARGUMENT_SIZE        = 7010;

const int SERVER_TERMINATION_FAILURE   = 7011;

const int UNEXPORT_FAILURE             = 7012;

const int REMOTE_CALL_ERROR            = 7013;

const int XML_BUILDING_ERROR           = 7014;

const int INVALID_ARGUMENT_VALUE       = 7015;

const int XML_PARSING_ERROR            = 7016;
#endif /*__SOAP_ERROR_CODES_H__*/
