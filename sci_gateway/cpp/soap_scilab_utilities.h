/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_SCILAB_UTILITIES_H__
#define __SOAP_SCILAB_UTILITIES_H__

/**
 * Checks if the name belongs to a Scilab primitive
 * @param[in] name the string to be checked
 * @return true if the name belongs to a Scilab primitive, false otherwise
 */
bool isScilabPrimitive(char* name);

/**
 * Checks if the name belongs to a Scilab macro defined in a library
 * @param[in] name the string to be checked
 * @return true if the name belongs to a Scilab macro, false otherwise
 */
bool isScilabLibraryMacro(char* name);

/**
 * Checks if the name belongs to a Scilab macro defined in the console
 * @param[in] name the string to be checked
 * @return true if the name belongs to a Scilab macro, false otherwise
 */
bool isScilabUserMacro(char* name);

#endif /*__SOAP_SCILAB_UTILITIES_H__*/
