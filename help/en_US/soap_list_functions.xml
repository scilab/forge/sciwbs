<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file is released into the public domain
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="soap_list_functions" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>soap_list_functions</refname>

    <refpurpose>displays the list of all functions currently exported to a given SOAP server</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>functions = soap_list_functions()</synopsis>
    <synopsis>functions = soap_list_functions(serverId)</synopsis>
    <synopsis>functions = soap_list_functions(hostname, port)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>

      <varlistentry>
        <term>serverId</term>

        <listitem>
          <para>a valid identifier of a running SOAP server</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>hostname</term>

        <listitem>
          <para>the host name of a remote SOAP server</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>port</term>

        <listitem>
          <para>the port of a remote SOAP server</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>functions</term>

        <listitem>
          <para>a column string vector containing the names of the exported functions</para>
        </listitem>
      </varlistentry>

    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Lists all functions that are currently available for remote access on a given SOAP server 
    (local or remote). In order to retrieve the list of functions from a remote SOAP server invokes
    a SOAP method on that server.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    <para>To retrive the list of functions from a local SOAP server (for example, with ID 4), execute the
    statement:</para>
    <programlisting role="example">soap_list_functions(4)</programlisting>

    <para>To see the list of functions exported to the default (i.e. the only running) local SOAP server, 
    type:</para>
    <programlisting role="example">soap_list_functions()</programlisting>

    <para>The following command may be used to list the functions available from a remote SOAP server
    (host name "www.foobar.com", port 1234 in this example):</para>
    <programlisting role="example">soap_list_functions('www.foobar.com', 1234)</programlisting>


  </refsection>

  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="soap_export">soap_export</link>
      </member>
      <member>
        <link linkend="soap_unexport">soap_unexport</link>
      </member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Artem GLEBOV</member>
    </simplelist>
  </refsection>
</refentry>
