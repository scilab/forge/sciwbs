/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "gsoap/soapH.h"

#include "SOAP.h"

#include <list>
#include <string>

int ns1__listFunctions(struct soap* soap, struct ns1__listFunctionsResponse* result)
{
    // get the functions list
    std::list<std::string> functions = SOAP::Instance().listFunctions();

    // fill the response structure
    ns1__StringList *pList = new ns1__StringList();
    const int size = functions.size();
    pList->__sizestring = size;
    pList->string = new char*[size];
    std::list<std::string>::const_iterator it = functions.begin();
    for (int i = 0 ; it != functions.end(); it++, i++)
    {
        pList->string[i] = new char[it->size()];
        strcpy(pList->string[i], it->c_str());
    }

    // set the output parameter
    result->result = pList;

    return SOAP_OK;
}
