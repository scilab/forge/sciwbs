/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_EXCEPTIONS_H__
#define __SOAP_EXCEPTIONS_H__

class NoServerRunningException {};

class MultipleServersRunningException {};

class ServerNotFoundException
{
private:
    int serverId;
public:
    ServerNotFoundException(int sid) { serverId = sid; }
    int getServerId() { return serverId; }
};

class RemoteCallException {};

#endif /* __SOAP_EXCEPTIONS_H__ */
