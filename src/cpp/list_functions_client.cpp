/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "list_functions_client.h"
#include "soap_exceptions.h"

#include <stdio.h>
#include <string.h>

#include "gsoap/soapH.h"
//#include "gsoap/nsmap.h"

#include "utilities.h"

std::list<std::string> retrieveFunctionList(const char* const hostname, const int port)
{
    struct soap soap;
    soap_init(&soap);

    // get the http form of the address
    char* server = getHttpAddress(hostname, port);

    ns1__listFunctionsResponse response;

    // retrieve the list of functions
    if(soap_call_ns1__listFunctions(&soap, server, "", &response) != SOAP_OK)
    {
        //soap_sprint_fault(&soap, fault, 300);
        throw RemoteCallException();
    }

    std::list<std::string> functions;
    ns1__StringList* pList = response.result;
    for (int i = 0; i < pList->__sizestring; i++)
    {
        functions.push_back(pList->string[i]);
    }

    // free the allocated memory
    if(server != NULL)
    {
        delete server;
    }

    soap_destroy(&soap);
    soap_end(&soap);
    soap_done(&soap);

    return functions;
}
