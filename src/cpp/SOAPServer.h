/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __SOAP_SERVER_H__
#define __SOAP_SERVER_H__

#include <string>
#include <list>

#include "Thread_Wrapper.h"

#include "soap_server_exceptions.h"

class SOAPServer
{
private:
    std::string hostname;
    int port;

    std::list<std::string> functions;

    int state;  // 0 - stopped, 1 - running

    int returnValue;  // 0 - successful, otherwise - error

    __threadId threadId;
    __threadLock objectLock;

    __threadLock serverStartLock;
    __threadSignal serverStartSignal;

public:
    SOAPServer();
    ~SOAPServer();

    void run(const char* const ia, const int p);
    void terminate();

    void exportFunction(std::string& functionName);
    void exportFunction(char* functionName);

    void unexportFunction(std::string& functionName);
    void unexportFunction(char* functionName);

    std::list<std::string> getFunctions() const { return functions; }

    std::string getHostName() const { return hostname;  }
    int getPort()             const { return port;      }

    int getState()            const { return state;     }

    friend void* server_routine(void* arg);
};

#endif /* __SOAP_SERVER_H__ */
