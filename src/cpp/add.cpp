/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "add.h"

#include "gsoap/soapH.h"
#include "gsoap/nsmap.h"

#include "utilities.h"

int add(const char* const hostname, const int port, void * const a, void * const b,
        void ** const c)
{
    struct soap soap;
    soap_init(&soap);

    // get the http form of the address
    char* server = getHttpAddress(hostname, port);

    ns1__addResponse res;

    // Calculate the sum
    if(soap_call_ns1__add(&soap, server, "", a, b, &res) != SOAP_OK)
    {
        //soap_sprint_fault(&soap, fault, 300);
        return -1;
    }

    // assign the result
    *c = res.result;

    // remove from the gSOAP deallocation chain
    soap_unlink(&soap, res.result);

    // free the allocated memory
    if(server != NULL)
    {
        delete server;
    }

    soap_destroy(&soap);
    soap_end(&soap);
    soap_done(&soap);

    return 0;
}
