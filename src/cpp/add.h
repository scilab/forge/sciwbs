/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Artem GLEBOV <aglebov@acm.org>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __ADD_H__
#define __ADD_H__

/**
 * Performs the addition of two matrices calling a remote SOAP method for 
 * each pair of arguments
 * @param[in] hostname the host name of the SOAP server to use
 * @param[in] port the port of the SOAP server to use
 * @param[in] a the first summand
 * @param[in] b the second summand
 * @param[out] c the result of addition
 * @return 0 if successful, a negative value otherwise
 */
int add(const char* const hostname, const int port, void * const a, void * const b,
        void ** const c);

#endif /* __CSUM_H__ */

